//Pseudocodigo para ver el mayor y menor entre 3 numeros
Funcion Max_Min
	Definir espacio1 Como Entero
	Definir espacio2 Como Entero
	Definir espacio3 Como Entero
	Definir mayor Como Entero
    Definir menor Como Entero

    //(Opcional)Se puede inicializar los espacios en memoria. 

	Imprimir  'Ingrese el primer numero'
	Leer espacio1
	
	Imprimir  'Ingrese el segundo numero'
	Leer espacio2
	
	Imprimir  'Ingrese el tercer numero'
	Leer espacio3
	
	Si espacio1 > espacio2 Y espacio1 > espacio3
		mayor = espacio1
	SiNo
		Si espacio2 > espacio1 Y espacio2 > espacio3
			mayor = espacio2
		SiNo
			mayor = espacio3
		FinSi
	FinSi
	
    Si espacio1 < espacio2 Y espacio1 < espacio3
		menor = espacio1
	SiNo
		Si espacio2 < espacio1 Y espacio2 < espacio3
			menor = espacio2
		SiNo
			menor = espacio3
		FinSi
	FinSi

    Imprimir Concatenar('El mayor es: ', mayor)
    Imprimir Concatenar('El menor es: ', menor)
FinFuncion

////////////////////////////
//Otra manera de resolverlo/
////////////////////////////

Funcion Max_Min2
	Definir espacio1 Como Entero
	Definir espacio2 Como Entero
	Definir espacio3 Como Entero

	Imprimir  'Ingrese el primer numero'
	Leer espacio1
	
	Imprimir  'Ingrese el segundo numero'
	Leer espacio2
	
	Imprimir  'Ingrese el tercer numero'
	Leer espacio3
	
	Si espacio1 > espacio2 Y espacio1 > espacio3
	    Imprimir Concatenar('El mayor es: ', espacio1)
	SiNo
		Si espacio2 > espacio1 Y espacio2 > espacio3
			Imprimir Concatenar('El mayor es: ', espacio2)
		SiNo
			Imprimir Concatenar('El mayor es: ', espacio3)
		FinSi
	FinSi
	
    Si espacio1 < espacio2 Y espacio1 < espacio3
		Imprimir Concatenar('El mayor es: ', espacio1)
	SiNo
		Si espacio2 < espacio1 Y espacio2 < espacio3
			Imprimir Concatenar('El mayor es: ', espacio2)
		SiNo
			Imprimir Concatenar('El mayor es: ', espacio3)
		FinSi
	FinSi

    Imprimir Concatenar('El mayor es: ', mayor)
    Imprimir Concatenar('El menor es: ', menor)
FinFuncion

//Preguntas
/*
    1. De que otra forma puedo solucionar el problema? (no importa si es mas larga)
    2. Como puedo cambiar hacer mas eficiente este pseudocodigo?
    3. Que factor no se esta tomando en cuenta? 
*/