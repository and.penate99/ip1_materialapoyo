//Pseudocodigo para calcular la superficie y perimetro de un Circulo

Proceso Circulo
    Definir radio,superficie,perimetro como Real
    Imprimir "Introduce el radio de la circunferencia:"
    Leer radio
    superficie <- PI * radio ^ 2
    perimetro <- 2 * PI * radio
    Imprimir "La superficie es ",superficie
    Imprimir "El perímetro es ",perimetro
FinProceso