// El programa consiste en que el usuario intentar� adivinar
// el n�mero aleatorio que el programa ha generado.
// Se le solicitar� un numero al usuario hasta que sea igual al del programa.

Algoritmo AdivinaNumero
	Definir numeroAleatorio, numeroUsuario Como Entero
	numeroAleatorio <- azar(15) + 1
	
	Escribir "Ingresa un numero"
	Leer numeroUsuario
	
	Mientras numeroUsuario <> numeroAleatorio Hacer
		Escribir "Has fallado, ingresa otro numero"
		Leer numeroUsuario
	Fin Mientras
	
	Escribir "Felicidades! Has acertado!!"
FinAlgoritmo
