//Pseudocodigo para ver el mayor y menor entre 3 numeros
Funcion Sumas
	Definir espacio1 Como Entero
	Definir espacio2 Como Entero
	Definir resultado Como Entero

    //(Opcional)Se puede inicializar los espacios en memoria. 

	Imprimir  'Ingrese el primer numero'
	Leer espacio1
	
	Imprimir  'Ingrese el segundo numero'
	Leer espacio2
	
    resultado = espacio1 + espacio2

    Imprimir Concatenar('El resultado de la suma es: ', resultado)

FinFuncion
