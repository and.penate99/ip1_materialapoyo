// Arreglar el formato de una oracion
// 		Una oracion con un formato correcto empieza
// 		con letra inicial mayuscula y lo demas en minuscula.
// Faltan cosas por considerar pero se hara de manera simple.

Algoritmo FormatoOracion
	Definir oracion, primerLetra, resto, oracionCorrecta Como Caracter
	
	Escribir "Escribe una oracion"
	Leer oracion
	
	primerLetra <- SubCadena(oracion, 1, 1)
	resto <- SubCadena(oracion, 2, Longitud(oracion))
	
	oracionCorrecta <- Concatenar(Mayusculas(primerLetra), Minusculas(resto))
	Escribir oracionCorrecta
FinAlgoritmo
